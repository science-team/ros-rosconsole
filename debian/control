Source: ros-rosconsole
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders:
 Jochen Sprickerhof <jspricke@debian.org>,
 Leopold Palomo-Avellaneda <leo@alaxarxa.net>,
 Timo Röhling <roehling@debian.org>,
Section: libs
Priority: optional
Build-Depends:
 catkin (>= 0.8.10-1~),
 debhelper-compat (= 13),
 dh-ros,
 libboost-regex-dev,
 libboost-system-dev,
 libboost-thread-dev,
 libgtest-dev <!nocheck>,
 liblog4cxx-dev,
 libroscpp-core-dev,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://wiki.ros.org/rosconsole
Vcs-Browser: https://salsa.debian.org/science-team/ros-rosconsole
Vcs-Git: https://salsa.debian.org/science-team/ros-rosconsole.git

Package: librosconsole-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libboost-regex-dev,
 libboost-system-dev,
 libboost-thread-dev,
 liblog4cxx-dev,
 librosconsole3d (= ${binary:Version}),
 libroscpp-core-dev,
 ${misc:Depends},
Description: Development files for librosconsole
 This package is part of Robot OS (ROS). It is the ROS console output
 library, a C++ package that supports console output and logging in
 roscpp. It provides a macro-based interface which allows both printf-
 and stream-style output. It also wraps log4cxx, which supports
 hierarchical loggers, verbosity levels and configuration-files.
 .
 This package contains the development files for the library.

Package: librosconsole3d
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Multi-Arch: same
Description: library for librosconsole
 This package is part of Robot OS (ROS). It is the ROS console output
 library, a C++ package that supports console output and logging in
 roscpp. It provides a macro-based interface which allows both printf-
 and stream-style output. It also wraps log4cxx, which supports
 hierarchical loggers, verbosity levels and configuration-files.
 .
 This package contains the library.
